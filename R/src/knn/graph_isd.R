################################################################################
#	ENVIRONNEMENT															   #
################################################################################
# supprime toutes les données conservées dans l'environnement
source("util/env.R")
env.env$set_workdir(clear.env = TRUE)



################################################################################
#	IMPORT DES SCRIPTS														   #
################################################################################
# Liste des scripts R utilisés
.utils <- c("isd_graph.R")

lapply(
  X = .utils,
  FUN = function(x) {
    source(
      env.env$path(
        root.directory = env.env$DIRECTORY.UTIL,  
        filename = x
      )
    )
  }
)




################################################################################
#	LIBRAIRIES																   #
################################################################################
# Liste les librairies nécessaires. "readr","caret"
.packages <- c("readr","e1071","viridis","hrbrthemes","ggplot2")

# Vérification de l'installation des librairies. Dans le cas où elles ne sont 
# pas installées, alors elles sont téléchargées, installées puis chargées. Dans 
# le cas contraire, elles sont directement chargées.
lapply(
  X = .packages,
  FUN = function(x) {
    if (!require(x, character.only = TRUE)) {
      install.packages(x, dependencies = TRUE)
      library(x, character.only = TRUE)
    }
  }
)



################################################################################
#	IMPORT DE DONNÉES														   #
################################################################################
# retourne les données du fichier csv fournie en paramètre.
import_data <- function(file_path) {
  return(
    read_csv2(
      file = file_path, 
      col_types = list(
        .default = col_integer(),
        Package = col_character(),
        Category = col_character()
      )
    )
  )
}

# importe le jeux de données "Mendeley Data"
dataset <- import_data(
  file_path = env.env$path(
    root.directory = env.env$DIRECTORY.DATA, 
    sub.directory = "partial/mendeleydata/feature", 
    filename = "All.csv"
  )
)

# créer un tableau indexé de données statistique. Contient le nombre 
# d'application par classe (benign/malware) ainsi que les indices de début et
# de fin de bloc des jeux d'entrainement, de test et du reste.
isd <- env.isd$isd(dataset = dataset, train.percent = 70)

mat <- isd@statistics

################################################################################
#	 KNN														   #
################################################################################

# Fonction qui calcul le pourcentage réussi
# x: une catégorie 
# valeur_k: un k
# value_distance: une  méthode de distance
err_cat_k <- function(x,valeur_k,value_distance) { 
  result = tryCatch({
    print(x)
    benign_train <- env.isd$isd_dataset(isd,x,env.isd$BLOCK.TRAIN,FALSE)
    malware_train <- env.isd$isd_dataset(isd,x,env.isd$BLOCK.TRAIN,TRUE)
    
    benign_test <- env.isd$isd_dataset(isd,x,env.isd$BLOCK.TEST,FALSE)
    malware_test <-env.isd$isd_dataset(isd,x,env.isd$BLOCK.TEST,TRUE)
    
    class_train <- append(benign_train$Class ,  malware_train$Class)
    class_test <- append(benign_test$Class,malware_test$Class)
    
    tab_train <- rbind(benign_train , malware_train)[,isd@caracteristics]
    tab_test <- rbind(benign_test ,malware_test)[,isd@caracteristics]
    
    model <- gknn(x=tab_train ,factor(class_train), k = valeur_k,method = value_distance)
    pred_test <- predict(model, tab_test, type = "class")
    err_test = sum(pred_test!=class_test)/length(class_test)
    
    return (100-round((err_test*100), digits = 0))
  }, warning = function(w) {
    return (NA)
  }, error = function(e) {
    return (NA)
  })
}



# Fonction qui calcul le pourcentage réussi pour toutes les categories et les k
# x: une catégorie 
# value_distance: une  méthode de distance
err_dif_k <- function(x,value_distance,k) { 
  return(sapply(k,function(u) err_cat_k(x,u,value_distance)))
}

knnF <- function(k,distance) { 
  c = isd@categories
  text_k = lapply(k,function(u) paste("k = ",u))
  res2 = sapply(c,function(u) err_dif_k(u,distance,k))
  rownames(res2) = k
  colnames(res2) = c
  return (res2)
}

valeur_k <- seq(1, 52, by=2)
distance = c("euclidean","jaccard","Cosine")


# Knn avec des methodes différentes
euclidean <- knnF(valeur_k,distance[1])
jaccard <- knnF(valeur_k,distance[2])
cosine <- knnF(valeur_k,distance[3])

l = isd@categories
data <- expand.grid(k = valeur_k, categorie = l)
View(data)
data$Euclidean <- apply(
  X = data, 
  MARGIN = 1, 
  FUN = function(x) {
    val1 = as.numeric( x["k"])
    val = which(valeur_k== val1)
    return (euclidean[val,x["categorie"]])
  }
)

data$Jaccard <- apply(
  X = data, 
  MARGIN = 1, 
  FUN = function(x) {
    val1 = as.numeric( x["k"])
    val = which(valeur_k== val1)
    return (jaccard[val,x["categorie"]])
  }
)

data$Cosine <- apply(
  X = data, 
  MARGIN = 1, 
  FUN = function(x) {
    val1 = as.numeric( x["k"])
    val = which(valeur_k== val1)
    return (cosine[val,x["categorie"]])
  }
)

graph <- ggplot( data,aes(x = k )) +
  geom_line(aes(y=Euclidean,group = categorie,color='Euclidean')) +
  geom_line(aes(y=Jaccard,group = categorie,color = 'Jaccard')) +
  geom_line(aes(y=Cosine,group = categorie,color = 'Cosine')) +
  labs(y = "prédictions réussites") +
  theme(
    plot.background = element_rect(fill = "#2f3136", color = "#2f3136"),
    text = element_text(color = "#ecebed"),
    axis.text = element_text(color = "#ecebed"),
    strip.background = element_rect(fill = "#58596c", color = "#58596c"),
    strip.text = element_text(color = "#ecebed"),
    panel.background = element_rect(fill = "#292b30"),
    panel.grid = element_line(color = "#00000033"),
    panel.spacing = unit(1, "lines"),
    legend.background = element_rect("#2f3136ff"),
    legend.key = element_rect(fill = "#2f3136ff")
  ) +
  labs(x = "k",color = "Distance") +
  facet_wrap(~categorie)


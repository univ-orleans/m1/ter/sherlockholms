# Data

**doc**

Contient les informations sur chaque attribut de notre jeu de données.

**final**

Contient l'ensemble des jeux de données après avoir tout nettoyé.

**partial**

Contient une partie des jeux de données.

**raw**

Contient les jeux de données brut.

**script**

Contient les scripts pour analyser les jeux de données.

**store**

Contient la liste des données en liens avec les applications des jeux de données

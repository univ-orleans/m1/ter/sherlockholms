# Statistiques

**Source :** [Mendeley Data](https://data.mendeley.com/datasets/b4mxg7ydb7/3)  
**Années :** 2017-2019
**Type :** Application Android
**Format :** `xlsx` transformé en `csv`

---

# Jeux de données
## Google Play Store
| Fichier csv | Type | Taille | Lignes | Colonnes | Doublons | Lignes vides | Commentaire |
| :-- | :-- | --: | --: | --: | :--: | :--: | :-- |
| Google Play Store   | original | 277 Mo | 99 999 | 1 439 | oui | oui |  |
| Google Play Store 1 | dérivé   | 106 Mo | 38 145 | 1 439 | non | non | Contient la colonne "Total". Le reste est identique. |
| Google Play Store 2 | dérivé   | 106 Mo | 38 145 | 1 438 | non | non | Ne contient pas la colonne "Total". Le reste est identique. |

<br/><br/>

## Malware Application
| Fichier csv | Type | Taille | Lignes | Colonnes | Doublons | Lignes vides | Commentaire |
| :-- | :-- | --: | --: | --: | :--: | :--: | :-- |
| Malware Application   | original | 103 Mo | 22 899 | 1 438 | oui | oui |  |
| Malware Application 1 | dérivé   | 16 Mo  |  8 754 | 1 438 | non | non |  |
| Malware Application 2 | dérivé   | 16 Mo  |  8 754 | 1 438 | non | non |  |

<br/><br/>

## Third Party
| Fichier csv | Type | Taille | Lignes | Colonnes | Doublons | Lignes vides | Commentaire |
| :-- | :-- | --: | --: | --: | :--: | :--: | :-- |
| Third Party   | original | 82 Mo | 11 192 | 1 438 | oui | oui |  |
| Third Party 1 | dérivé   | 20 Mo | 10 716 | 1 438 | non | non |  |

<br/><br/>

# Accesibilité des apk
Tableau récapitulatif du nombre d'application Android encore disponnible sur le Google Play en 2021.
| Fichier csv | Nombre de package | Présents sur Google Play | Non présent sur Google Play |
| :-- | --: | --: | --: |
| Google Play Store 	| 49 999 | <span style="white-space:pre;font-family:monospace">9 727 <span style="color:#888888">[ 20%]</span></span> | <span style="white-space:pre;font-family:monospace">40 271 <span style="color:#888888">[ 80%]</span></span> |
| Malware Application	|  9 999 | <span style="white-space:pre;font-family:monospace">2 737 <span style="color:#888888">[ 27%]</span></span> |  <span style="white-space:pre;font-family:monospace">7 261 <span style="color:#888888">[ 73%]</span></span> |
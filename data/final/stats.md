# Statistiques des jeux de données



## Caractéristiques général
| # | Jeu de donnée | Bénigne | Malveillante | Total |
| --: | :-- | --: | --: | --: |
| **1** | Mendeley Data |<span style="white-space:pre;font-family:monospace">4 106<span style="color:#888888"> [ 32%]</span></span>|<span style="white-space:pre;font-family:monospace">8 772<span style="color:#888888">  [ 68%]</span></span>|<span style="white-space:pre;font-family:monospace">12 878</span>|
| **2** | FigShare      |<span style="white-space:pre;font-family:monospace">16 816<span style="color:#888888"> [ 66%]</span></span>|<span style="white-space:pre;font-family:monospace">8 643<span style="color:#888888">  [ 34%]</span></span>|<span style="white-space:pre;font-family:monospace">25 459</span>|

<br/><br/><br/>

## Caractéristique par catégorie
### Jeu de données "Mendeley Data"
| # | Catégorie | Fichier `Benign.csv` | Fichier `Malware.csv` | Fichier `All.csv` |
| --: | :-- | --: | --: | --: |
|  **1** | Arcade & Action		|<span style="white-space:pre;font-family:monospace">113<span style="color:#888888"> [ 14%]</span></span>|<span style="white-space:pre;font-family:monospace">671<span style="color:#888888">  [ 86%]</span></span>| 784  |
|  **2** | Books & Reference	|<span style="white-space:pre;font-family:monospace">192<span style="color:#888888"> [ 36%]</span></span>|<span style="white-space:pre;font-family:monospace">342<span style="color:#888888">  [ 64%]</span></span>| 534  |
|  **3** | Brain & Puzzle		|<span style="white-space:pre;font-family:monospace">146<span style="color:#888888"> [ 23%]</span></span>|<span style="white-space:pre;font-family:monospace">493<span style="color:#888888">  [ 77%]</span></span>| 639  |
|  **4** | Business				|<span style="white-space:pre;font-family:monospace">133<span style="color:#888888"> [ 42%]</span></span>|<span style="white-space:pre;font-family:monospace">183<span style="color:#888888">  [ 58%]</span></span>| 316  |
|  **5** | Cards & Casino		|<span style="white-space:pre;font-family:monospace"> 99<span style="color:#888888"> [ 36%]</span></span>|<span style="white-space:pre;font-family:monospace">173<span style="color:#888888">  [ 64%]</span></span>| 272  |
|  **6** | Casual				|<span style="white-space:pre;font-family:monospace">117<span style="color:#888888"> [ 27%]</span></span>|<span style="white-space:pre;font-family:monospace">320<span style="color:#888888">  [ 73%]</span></span>| 437  |
|  **7** | Comics				|<span style="white-space:pre;font-family:monospace"> 25<span style="color:#888888"> [ 24%]</span></span>|<span style="white-space:pre;font-family:monospace"> 80<span style="color:#888888">  [ 76%]</span></span>| 105  |
|  **8** | Communication		|<span style="white-space:pre;font-family:monospace">115<span style="color:#888888"> [ 24%]</span></span>|<span style="white-space:pre;font-family:monospace">355<span style="color:#888888">  [ 76%]</span></span>| 470  |
|  **9** | Education			|<span style="white-space:pre;font-family:monospace">218<span style="color:#888888"> [ 39%]</span></span>|<span style="white-space:pre;font-family:monospace">335<span style="color:#888888">  [ 61%]</span></span>| 553  |
| **10** | Entertainment		|<span style="white-space:pre;font-family:monospace">161<span style="color:#888888"> [ 19%]</span></span>|<span style="white-space:pre;font-family:monospace">670<span style="color:#888888">  [ 81%]</span></span>| 831  |
| **11** | Finance				|<span style="white-space:pre;font-family:monospace">236<span style="color:#888888"> [ 51%]</span></span>|<span style="white-space:pre;font-family:monospace">225<span style="color:#888888">  [ 49%]</span></span>| 461  |
| **12** | Health & Fitness		|<span style="white-space:pre;font-family:monospace">108<span style="color:#888888"> [ 37%]</span></span>|<span style="white-space:pre;font-family:monospace">181<span style="color:#888888">  [ 63%]</span></span>| 289  |
| **13** | Libraries & Demo		|<span style="white-space:pre;font-family:monospace"> 55<span style="color:#888888"> [ 62%]</span></span>|<span style="white-space:pre;font-family:monospace"> 34<span style="color:#888888">  [ 38%]</span></span>| 89   |
| **14** | Lifestyle			|<span style="white-space:pre;font-family:monospace">221<span style="color:#888888"> [ 38%]</span></span>|<span style="white-space:pre;font-family:monospace">366<span style="color:#888888">  [ 62%]</span></span>| 587  |
| **15** | Media & Video		|<span style="white-space:pre;font-family:monospace"> 83<span style="color:#888888"> [ 30%]</span></span>|<span style="white-space:pre;font-family:monospace">191<span style="color:#888888">  [ 70%]</span></span>| 274  |
| **16** | Medical				|<span style="white-space:pre;font-family:monospace"> 98<span style="color:#888888"> [ 72%]</span></span>|<span style="white-space:pre;font-family:monospace"> 39<span style="color:#888888">  [ 28%]</span></span>| 137  |
| **17** | Music & Audio		|<span style="white-space:pre;font-family:monospace">152<span style="color:#888888"> [ 39%]</span></span>|<span style="white-space:pre;font-family:monospace">241<span style="color:#888888">  [ 61%]</span></span>| 393  |
| **18** | News & Magazines		|<span style="white-space:pre;font-family:monospace">193<span style="color:#888888"> [ 49%]</span></span>|<span style="white-space:pre;font-family:monospace">200<span style="color:#888888">  [ 51%]</span></span>| 393  |
| **19** | Personalization		|<span style="white-space:pre;font-family:monospace">136<span style="color:#888888"> [ 19%]</span></span>|<span style="white-space:pre;font-family:monospace">587<span style="color:#888888">  [ 81%]</span></span>| 723  |
| **20** | Photography			|<span style="white-space:pre;font-family:monospace"> 54<span style="color:#888888"> [ 29%]</span></span>|<span style="white-space:pre;font-family:monospace">131<span style="color:#888888">  [ 71%]</span></span>| 185  |
| **21** | Productivity			|<span style="white-space:pre;font-family:monospace">178<span style="color:#888888"> [ 25%]</span></span>|<span style="white-space:pre;font-family:monospace">537<span style="color:#888888">  [ 75%]</span></span>| 715  |
| **22** | Racing				|<span style="white-space:pre;font-family:monospace"> 25<span style="color:#888888"> [ 18%]</span></span>|<span style="white-space:pre;font-family:monospace">117<span style="color:#888888">  [ 82%]</span></span>| 142  |
| **23** | Shopping				|<span style="white-space:pre;font-family:monospace">148<span style="color:#888888"> [ 44%]</span></span>|<span style="white-space:pre;font-family:monospace">191<span style="color:#888888">  [ 56%]</span></span>| 339  |
| **24** | Social				|<span style="white-space:pre;font-family:monospace"> 64<span style="color:#888888"> [ 26%]</span></span>|<span style="white-space:pre;font-family:monospace">179<span style="color:#888888">  [ 74%]</span></span>| 243  |
| **25** | Sports				|<span style="white-space:pre;font-family:monospace">147<span style="color:#888888"> [ 40%]</span></span>|<span style="white-space:pre;font-family:monospace">225<span style="color:#888888">  [ 60%]</span></span>| 372  |
| **26** | Sports Games			|<span style="white-space:pre;font-family:monospace"> 53<span style="color:#888888"> [ 30%]</span></span>|<span style="white-space:pre;font-family:monospace">122<span style="color:#888888">  [ 70%]</span></span>| 175  |
| **27** | Tools				|<span style="white-space:pre;font-family:monospace">338<span style="color:#888888"> [ 26%]</span></span>|<span style="white-space:pre;font-family:monospace">946<span style="color:#888888">  [ 74%]</span></span>| 1284 |
| **28** | Transportation		|<span style="white-space:pre;font-family:monospace">139<span style="color:#888888"> [ 58%]</span></span>|<span style="white-space:pre;font-family:monospace">100<span style="color:#888888">  [ 42%]</span></span>| 239  |
| **29** | Travel & Local		|<span style="white-space:pre;font-family:monospace">194<span style="color:#888888"> [ 31%]</span></span>|<span style="white-space:pre;font-family:monospace">438<span style="color:#888888">  [ 69%]</span></span>| 632  |
| **30** | Weather				|<span style="white-space:pre;font-family:monospace">165<span style="color:#888888"> [ 62%]</span></span>|<span style="white-space:pre;font-family:monospace">100<span style="color:#888888">  [ 38%]</span></span>| 265  |

<br/><br/>

### Jeu de données "FigShare"
| # | Catégorie | Fichier `Benign.csv` | Fichier `Malware.csv` | Fichier `All.csv` |
| --: | :-- | --: | --: | --: |
|  **1** | Arcade & Action		|<span style="white-space:pre;font-family:monospace">  539<span style="color:#888888"> [ 45%]</span></span>|<span style="white-space:pre;font-family:monospace">665<span style="color:#888888">  [ 55%]</span></span>| 1204 |
|  **2** | Books & Reference	|<span style="white-space:pre;font-family:monospace">  521<span style="color:#888888"> [ 61%]</span></span>|<span style="white-space:pre;font-family:monospace">337<span style="color:#888888">  [ 39%]</span></span>| 858  |
|  **3** | Brain & Puzzle		|<span style="white-space:pre;font-family:monospace">  547<span style="color:#888888"> [ 53%]</span></span>|<span style="white-space:pre;font-family:monospace">488<span style="color:#888888">  [ 47%]</span></span>| 1035 |
|  **4** | Business				|<span style="white-space:pre;font-family:monospace">  518<span style="color:#888888"> [ 75%]</span></span>|<span style="white-space:pre;font-family:monospace">175<span style="color:#888888">  [ 25%]</span></span>| 693  |
|  **5** | Cards & Casino		|<span style="white-space:pre;font-family:monospace">  556<span style="color:#888888"> [ 76%]</span></span>|<span style="white-space:pre;font-family:monospace">171<span style="color:#888888">  [ 24%]</span></span>| 727  |
|  **6** | Casual				|<span style="white-space:pre;font-family:monospace">  677<span style="color:#888888"> [ 68%]</span></span>|<span style="white-space:pre;font-family:monospace">312<span style="color:#888888">  [ 32%]</span></span>| 989  |
|  **7** | Comics				|<span style="white-space:pre;font-family:monospace">  506<span style="color:#888888"> [ 87%]</span></span>|<span style="white-space:pre;font-family:monospace"> 78<span style="color:#888888">  [ 13%]</span></span>| 584  |
|  **8** | Communication		|<span style="white-space:pre;font-family:monospace">  543<span style="color:#888888"> [ 61%]</span></span>|<span style="white-space:pre;font-family:monospace">351<span style="color:#888888">  [ 39%]</span></span>| 894  |
|  **9** | Education			|<span style="white-space:pre;font-family:monospace">  511<span style="color:#888888"> [ 60%]</span></span>|<span style="white-space:pre;font-family:monospace">335<span style="color:#888888">  [ 40%]</span></span>| 846  |
| **10** | Entertainment		|<span style="white-space:pre;font-family:monospace">  568<span style="color:#888888"> [ 46%]</span></span>|<span style="white-space:pre;font-family:monospace">658<span style="color:#888888">  [ 54%]</span></span>| 1226 |
| **11** | Finance				|<span style="white-space:pre;font-family:monospace">  513<span style="color:#888888"> [ 70%]</span></span>|<span style="white-space:pre;font-family:monospace">223<span style="color:#888888">  [ 30%]</span></span>| 736  |
| **12** | Health & Fitness		|<span style="white-space:pre;font-family:monospace">  522<span style="color:#888888"> [ 74%]</span></span>|<span style="white-space:pre;font-family:monospace">180<span style="color:#888888">  [ 26%]</span></span>| 702  |
| **13** | Libraries & Demo		|<span style="white-space:pre;font-family:monospace">  506<span style="color:#888888"> [ 94%]</span></span>|<span style="white-space:pre;font-family:monospace"> 32<span style="color:#888888">  [  6%]</span></span>| 538  |
| **14** | Lifestyle			|<span style="white-space:pre;font-family:monospace">  515<span style="color:#888888"> [ 59%]</span></span>|<span style="white-space:pre;font-family:monospace">363<span style="color:#888888">  [ 41%]</span></span>| 878  |
| **15** | Media & Video		|<span style="white-space:pre;font-family:monospace">  530<span style="color:#888888"> [ 74%]</span></span>|<span style="white-space:pre;font-family:monospace">186<span style="color:#888888">  [ 26%]</span></span>| 716  |
| **16** | Medical				|<span style="white-space:pre;font-family:monospace">  508<span style="color:#888888"> [ 93%]</span></span>|<span style="white-space:pre;font-family:monospace"> 39<span style="color:#888888">  [  7%]</span></span>| 547  |
| **17** | Music & Audio		|<span style="white-space:pre;font-family:monospace">  534<span style="color:#888888"> [ 69%]</span></span>|<span style="white-space:pre;font-family:monospace">240<span style="color:#888888">  [ 31%]</span></span>| 774  |
| **18** | News & Magazines		|<span style="white-space:pre;font-family:monospace">  514<span style="color:#888888"> [ 72%]</span></span>|<span style="white-space:pre;font-family:monospace">199<span style="color:#888888">  [ 28%]</span></span>| 713  |
| **19** | Personalization		|<span style="white-space:pre;font-family:monospace">1 249<span style="color:#888888"> [ 68%]</span></span>|<span style="white-space:pre;font-family:monospace">584<span style="color:#888888">  [ 32%]</span></span>| 1833 |
| **20** | Photography			|<span style="white-space:pre;font-family:monospace">  526<span style="color:#888888"> [ 80%]</span></span>|<span style="white-space:pre;font-family:monospace">130<span style="color:#888888">  [ 20%]</span></span>| 656  |
| **21** | Productivity			|<span style="white-space:pre;font-family:monospace">  578<span style="color:#888888"> [ 52%]</span></span>|<span style="white-space:pre;font-family:monospace">533<span style="color:#888888">  [ 48%]</span></span>| 1111 |
| **22** | Racing				|<span style="white-space:pre;font-family:monospace">  504<span style="color:#888888"> [ 81%]</span></span>|<span style="white-space:pre;font-family:monospace">115<span style="color:#888888">  [ 19%]</span></span>| 619  |
| **23** | Shopping				|<span style="white-space:pre;font-family:monospace">  508<span style="color:#888888"> [ 73%]</span></span>|<span style="white-space:pre;font-family:monospace">190<span style="color:#888888">  [ 27%]</span></span>| 698  |
| **24** | Social				|<span style="white-space:pre;font-family:monospace">  534<span style="color:#888888"> [ 75%]</span></span>|<span style="white-space:pre;font-family:monospace">175<span style="color:#888888">  [ 25%]</span></span>| 709  |
| **25** | Sports				|<span style="white-space:pre;font-family:monospace">  579<span style="color:#888888"> [ 72%]</span></span>|<span style="white-space:pre;font-family:monospace">222<span style="color:#888888">  [ 28%]</span></span>| 801  |
| **26** | Sports Games			|<span style="white-space:pre;font-family:monospace">  512<span style="color:#888888"> [ 82%]</span></span>|<span style="white-space:pre;font-family:monospace">116<span style="color:#888888">  [ 18%]</span></span>| 628  |
| **27** | Tools				|<span style="white-space:pre;font-family:monospace">  647<span style="color:#888888"> [ 41%]</span></span>|<span style="white-space:pre;font-family:monospace">920<span style="color:#888888">  [ 59%]</span></span>| 1567 |
| **28** | Transportation		|<span style="white-space:pre;font-family:monospace">  508<span style="color:#888888"> [ 84%]</span></span>|<span style="white-space:pre;font-family:monospace"> 96<span style="color:#888888">  [ 16%]</span></span>| 604  |
| **29** | Travel & Local		|<span style="white-space:pre;font-family:monospace">  514<span style="color:#888888"> [ 54%]</span></span>|<span style="white-space:pre;font-family:monospace">430<span style="color:#888888">  [ 46%]</span></span>| 944  |
| **30** | Weather				|<span style="white-space:pre;font-family:monospace">  529<span style="color:#888888"> [ 84%]</span></span>|<span style="white-space:pre;font-family:monospace">100<span style="color:#888888">  [ 16%]</span></span>| 629  |

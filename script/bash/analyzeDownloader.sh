#!/bin/bash


#############################################################################
# FUNCTIONS																	#
#############################################################################
#####################################
# System							#
#####################################
# tue le processus via son PID
function ForcedStop() {
	local trigger="it self";
	
	if [[ $# -eq 1 ]]; then
		trigger=$1;
	fi;

	# écrire dans le journal
	Log "KILL" "PROCESS" "INFO" "force to kill main process and subprocesses.";

	# calculer le nomnbre de processus en cours d'exécution
	SUB_PROCESS=$(ps --ppid $$ -o comm | grep $(basename $0) -o | wc -w);

	# afficher un message dans la console (sortie standart) avant de forcer l'arrêt du programme
	echo -e "\nforced program to stop by $trigger.";
	echo "To resume, please execute this command: $0 $URL_FILE $API_KEY_FILE $(($i + $START_LINE - $SUB_PROCESS + 1)) $END_LINE";
	
	# supprimer le fichier temporaire
	rm -f $LIMIT_COUNT_FILE;
	kill -9 $$;
}



#####################################
# Env								#
#####################################
# récupère les variables d'environnement qui sont dans le fichier ".env"
function GetVar() {
	echo $(grep $1 $ENV | cut -d "=" -f 2- | xargs);
}



#####################################
# Log								#
#####################################
# écrit dans le fichier de log les actions en rapport direct avec les apk
function LogApk() {
	local ACTION=$1;
	local TAG=$2;
	local ACTION_STATUS=$3;
	local MESSAGE=$4;
	local SIZE=$(echo -n $URL_FILE_TOTAL_CONTENT_SIZE | wc -c);
	local INDEX=$(printf "%"$SIZE"s" $INDEX);

	if [[ ! -v HTTP_STATUS ]]; then
		HTTP_STATUS="---";
	fi;

	FORMAT="[$(date +"%F %T")] $HTTP_STATUS %-8s %-8s %-7s |%"$SIZE"s/%"$SIZE"s| %-50s \"$MESSAGE\"\n";
	printf "$FORMAT" $ACTION $TAG $ACTION_STATUS $INDEX $URL_FILE_TOTAL_CONTENT_SIZE $PACKAGE_NAME >> $LOG_FILE;
}


# écrit dans le fichier de log les actions en rapport direct avec les apk
function Log() {
	local ACTION=$1;
	local TAG=$2;
	local ACTION_STATUS=$3;
	local MESSAGE=$4;
	local SIZE=$(echo -n $URL_FILE_TOTAL_CONTENT_SIZE | wc -c);
	SIZE=$(echo "$SIZE * 2 + 3" | bc);
	
	if [[ ! -v HTTP_STATUS ]]; then
		HTTP_STATUS="---";
	fi;

	FORMAT="[$(date +"%F %T")] $HTTP_STATUS %-8s %-8s %-7s %$SIZE""s %-50s \"$MESSAGE\"\n";
	printf "$FORMAT" $ACTION $TAG $ACTION_STATUS " " >> $LOG_FILE;
}



#####################################
# Lock								#
#####################################
# retourne le nombre de requêtes restantes (stocké dans le fichier vérouillé)
function ReadRemainingRequest() {
	flock -x $LIMIT_COUNT_FILE cat $LIMIT_COUNT_FILE;
}


# enregistre dans le fichier vérouillé le nombre de requête restante
function WriteRemainingRequest() {
	flock -x $LIMIT_COUNT_FILE echo $1 > $LIMIT_COUNT_FILE;
}



#####################################
# Internet Request					#
#####################################
# télécharge les apk Android à partir d'une URL
function DownloadApk() {
	
	# vérification que l'apk n'est pas déjà stoké en local
	if [[ ! -e $PACKAGE ]]; then
		
		# télécharge l'apk
		HTTP_STATUS=$(curl -s --request GET $URL -o $PACKAGE -w "%{http_code}");
		
		case $HTTP_STATUS in
			200) 
				# succès
				# écrire dans le journal
				LogApk "DOWNLOAD" "APK" "INFO" "apk downloaded.";

				# démérage du processus de téléchargement de l'analyse de l'apk
				UploadApk &
				return 0;
			;;
			302)
				# changement de l'URI en cours
				# écrire dans le journal
				LogApk "DOWNLOAD" "APK" "WARNING" "apk downloaded, URI changing."

				# démérage du processus de téléchargement de l'analyse de l'apk
				UploadApk &
				return 0;
			;;
			*)
				# erreur
				# écrire dans le journal
				LogApk "DOWNLOAD" "APK" "ERROR" "apk not downloaded.";
				return 1;
			;;
		esac;
	else
		# écrire dans le journal
		LogApk "DOWNLOAD" "APK" "INFO" "apk already exist.";

		# démérage du processus de téléchargement de l'analyse de l'apk
		UploadApk &
		return 0;
	fi;
}



#####################################
# API								#
#####################################
# récupère depuis l'API le nombre de requête restante pour la journée
function DownloadRemainingRequest() {
	
	# récupérer le nombre de requête restante pour la journée
	local RESPONSE=$(curl -s --request GET https://www.virustotal.com/api/v3/users/$API_KEY --header "x-apikey:$API_KEY" -w " %{http_code}");

	# récupérer les informations de la réponse
	local HTTP_STATUS=$(echo $RESPONSE | rev | cut -d " " -f 1 | rev);
	local RESPONSE_BODY=$(echo $RESPONSE | rev | cut -d " " -f 2- | rev);

	case $HTTP_STATUS in
		200)
			# succès
			# récupération des informations de la réponse
			local ALLOWED=$(echo $RESPONSE_BODY | sed -e "s/ *//g" | sed -e "s/.*api_requests_daily//g" | sed -e "s/monitor_storage_bytes.*//g" | cut -d ":" -f 4 | cut -d "}" -f 1);
			local USED=$(echo $RESPONSE_BODY | sed -e "s/ *//g" | sed -e "s/.*api_requests_daily//g" | sed -e "s/monitor_storage_bytes.*//g" | cut -d ":" -f 3 | cut -d "," -f 1);
			
			# modifier la variable globale
			REMAINING_REQUESTS=$(($ALLOWED - $USED));

			# affichage dans la console (sortie standart)
			echo "Virustotal API:";
			echo "api-key: $API_KEY";
			echo "remaining request: $REMAINING_REQUESTS";

			# écrire dans le journal
			Log "DOWNLOAD" "LIMIT" "INFO" "limit downloaded, $REMAINING_REQUESTS requests can be used.";
		;;
		*)
			# erreur
			# écrire dans le journal
			Log "DOWNLOAD" "LIMIT" "ERROR" "limit not downloaded.";
		;;
	esac;
}


# demande au serveur de réaliser une analyse d'un apk Android
function UploadApk() {
	
	# définir l'URL (par défaut) de la reqûete
	local URL="https://www.virustotal.com/api/v3/files";

	# vérifier que l'apk n'est pas trop volumineux pour l'URL actuelle
	if [[ $(stat -c %s $PACKAGE) -ge $FILE_TRANSFER_SIZE_LIMIT ]]; then

		# récupérer une URL spécial opur les fichiers volumineux
		local RESPONSE=$(curl -s --request GET https://www.virustotal.com/api/v3/files/upload_url --header "x-apikey:$API_KEY" -w " %{http_code}");
		
		# récupérer les informations de la réponse
		local HTTP_STATUS=$(echo $RESPONSE | rev | cut -d " " -f 1 | rev);
		local RESPONSE_BODY=$(echo $RESPONSE | rev | cut -d " " -f 2- | rev);

		case $HTTP_STATUS in
			200)
				# succès
				# récupération de l'URL de transfer de gros volume
				URL=$(echo $RESPONSE_BODY | cut -d "\"" -f 4);

				# écrire dans le journal
				LogApk "DOWNLOAD" "URL" "INFO" "new url downloaded.";
			;;
			*)
				# erreur
				# écrire dans le journal
				LogApk "DOWNLOAD" "URL" "ERROR" "new url not downloaded.";

				# afficher le message d'erreur dans la console (sortie standart) avant d'arrêter l'exécution du programme
				echo "One error was occur with package ($INDEX/$URL_FILE_TOTAL_CONTENT_SIZE) names \"$PACKAGE_NAME\" at downloading new transfer URL. Error message: \"$RESPONSE_BODY\".";
				ForcedStop;
			;;
		esac;
	fi;

	# déposer l'apk sur le serveur via l'API
	local RESPONSE=$(curl -s --request POST $URL --header "x-apikey:$API_KEY" --form file=@$PACKAGE -w " %{http_code}");

	# mettre à jour le nombre de requête restante
	WriteRemainingRequest $(($(ReadRemainingRequest) - 1));

	# afficher dans la console (sortie standart)
	printf "%3s %s\n" $(ReadRemainingRequest) $PACKAGE_NAME;

	# récupérer les informations de la réponse
	local HTTP_STATUS=$(echo $RESPONSE | rev | cut -d " " -f 1 | rev);
	local RESPONSE_BODY=$(echo $RESPONSE | rev | cut -d " " -f 2- | rev);

	case $HTTP_STATUS in
		200)
			# succès
			# récupérer l'id de l'analyse
			local ID_ANALYSE=$(echo $RESPONSE_BODY | cut -d "\"" -f 10);

			# écrire dans le journal
			LogApk "UPLOAD" "APK" "INFO" "apk uploaded.";

			# attendre un temps approximatif le temp que l'analyse se termine
			sleep $TIME_BETWEEN_TWO_REQUEST"s";

			# lancement de la procédure de téléchargement du fichier d'analyse
			DownloadAnalyse;
		;;
		429)
			# limite de requête atteinte
			# écrire dans le journal
			LogApk "UPLOAD" "APK" "WARNING" "apk not uploaded, request limit reached.";
			
			# relancer la requête
			UploadApk;
		;;
		*)
			# erreur
			# récupérer le message d'erreur
			local ERROR_MESSAGE=$(echo $RESPONSE_BODY | cut -d "\"" -f 6);

			# écrire dans le journal
			LogApk "UPLOAD" "APK" "ERROR" "apk not uploaded, $ERROR_MESSAGE.";

			# afficher le message d'erreur dans la console (sortie standart) avant d'arrêter l'exécution du programme
			echo "One error was occur with package ($INDEX/$URL_FILE_TOTAL_CONTENT_SIZE) names \"$PACKAGE_NAME\" at sending Android app. Error message: \"$ERROR_MESSAGE\".";
			ForcedStop;
		;;
	esac;
}


# récupère depuis l'API le fichier d'analyse d'un apk
function DownloadAnalyse() {

	# récupérer l'analyse de l'application
	local RESPONSE=$(curl -s --request GET https://www.virustotal.com/api/v3/analyses/$ID_ANALYSE --header "x-apikey:$API_KEY" -w " %{http_code}");
	
	# récupérer les informations de la réponse
	local HTTP_STATUS=$(echo $RESPONSE | rev | cut -d " " -f 1 | rev);
	local RESPONSE_BODY=$(echo $RESPONSE | rev | cut -d " " -f 2- | rev);

	case $HTTP_STATUS in
		200)
			# succès
			local ANALYSIS_STATUS=$(echo $RESPONSE_BODY | cut -d "\"" -f 32);
			
			case $ANALYSIS_STATUS in
				"queued")
					# l'analyse de l'application est en cours
					# écrire dans le journal
					LogApk "DOWNLOAD" "ANALYSIS" "WARNING" "analysis not downloaded, it's in progress. wait "$TIME_BETWEEN_TWO_REQUEST"s before to try again.";
				
					# attendre un temps approximatif le temp que l'analyse se termine
					sleep $TIME_BETWEEN_TWO_REQUEST"s";

					# retanter de récupérer l'analyse
					DownloadAnalyse;
				;;
				"completed")
					# l'analyse de l'application est fini
					# écrire dans le journal
					LogApk "DOWNLOAD" "ANALYSIS" "INFO" "analysis is downloaded.";

					# récupérer les données du fichier d'analyse
					local TYPE_HARMLESS=$(echo $RESPONSE_BODY | cut -d ":" -f 14 | cut -d "," -f 1 | cut -d " " -f 2);
					local TYPE_SUSPICIOUS=$(echo $RESPONSE_BODY | cut -d ":" -f 16 | cut -d "," -f 1 | cut -d " " -f 2);
					local TYPE_MALICIOUS=$(echo $RESPONSE_BODY | cut -d ":" -f 20 | cut -d "," -f 1 | cut -d " " -f 2);
					local TYPE_UNDETECTED=$(echo $RESPONSE_BODY | cut -d ":" -f 21 | cut -d "," -f 1 | cut -d " " -f 2);

					# écriture des données dans le fichier de résultat
					echo "$PACKAGE_NAME;$TYPE_HARMLESS;$TYPE_SUSPICIOUS;$TYPE_MALICIOUS;$TYPE_UNDETECTED" >> $RESULT_FILE;

					# écrire dans le journal
					LogApk "WRITE" "ANALYSIS" "INFO" "data saved in file $RESULT_FILE.";

					# suppression de l'apk stocké dans le répertoire temporaire
					rm -f $PACKAGE;
				;;
			esac;
		;;
		429)
			# limite de requête atteinte
			# écrire dans le journal
			LogApk "DOWNLOAD" "ANALYSIS" "WARNING" "analysis not downloaded, request limit reach.";
			
			# relancer la requête
			DownloadAnalyse;
		;;
		*)
			# erreur
			# récupérer le message d'erreur
			local ERROR_MESSAGE=$(echo $RESPONSE_BODY | cut -d "\"" -f 6);

			# écrire dans le journal
			LogApk "DOWNLOAD" "ANALYSIS" "ERROR" "analysis not downloaded, $ERROR_MESSAGE.";

			# afficher le message d'erreur dans la console (sortie standart) avant d'arrêter l'exécution du programme
			echo "One error was occur with package ($INDEX/$URL_FILE_TOTAL_CONTENT_SIZE) names \"$PACKAGE_NAME\" at Android app analyse file downloading. Error message: \"$ERROR_MESSAGE\".";
			ForcedStop;
		;;
	esac;
}



#############################################################################
# RUN																		#
#############################################################################
#####################################
# System							#
#####################################
# capture le signale Ctrl+C pour forcer "proprement" l'arrêt du programme
trap "ForcedStop 'user'" 2;



#####################################
# Env								#
#####################################
# récupération du fichier d'environnement
readonly ENV=$(realpath $0 | rev | cut -d "/" -f 2- | rev)/.env;

# vérifie que le fichier d'environnement existe
if [[ ! -f $ENV ]]; then
	echo "env file not exist.";
	exit;
fi;



#####################################
# Data								#
#####################################
# le fichier source
declare -x URL_FILE;

# récupération du contenu du fichier d'url
declare -x URL_FILE_CONTENT;

# définition du nombre d'application à traiter
declare -x URL_FILE_TOTAL_CONTENT_SIZE;

# définir le nombre total d'application dans le fichier
declare -x URL_FILE_SUB_CONTENT_SIZE;

# le fichier de la clé de l'API
declare -x API_KEY_FILE;

# la clé de l'API
declare -x API_KEY;

# la première ligne à traiter
START_LINE=1;

# la dernière ligne à traiter
END_LINE=1;

# d'apk à traiter dans le même groupe
STEP=$(GetVar NUMBER_OF_APK_PROCESSED_AT_SAME_TIME);

# un nombre max d'erreur permis
ERROR_MAX=$STEP;

# définir le nombre d'erreur
ERROR=0;

# récupère le premier argument (le fichier csv d'URL)
if [[ $# -eq 0 ]]; then
	read -p "url csv file ? " URL_FILE;		
else
	URL_FILE=$1;
fi;

# vérifie que le fichier csv existe
if [[ ! -f $URL_FILE ]]; then
	echo "fichier \"$URL_FILE\" inexistant.";
	exit;
else
	URL_FILE_CONTENT=($(sed -n -e "$START_LINE,$(($(wc -l < $URL_FILE) + 1)) p" $URL_FILE));
	URL_FILE_TOTAL_CONTENT_SIZE=$(($START_LINE-1 + ${#URL_FILE_CONTENT[@]}));
	URL_FILE_SUB_CONTENT_SIZE=$URL_FILE_TOTAL_CONTENT_SIZE;
fi;

# récupère le second argument (la clé de l'API)
if [[ $# -le 1 ]]; then
	read -p "api key file ? " API_KEY_FILE;
else
	API_KEY_FILE=$2;
fi;

# vérifie que le fichier de la clé d'api existe
if [[ ! -f $API_KEY_FILE ]]; then
	echo "fichier \"$API_KEY_FILE\" inexistant.";
	exit;
else
	API_KEY=$(head -n 1 $API_KEY_FILE);
fi;

# récupère le troisième argument (la ligne de début)
if [[ $# -le 2 ]]; then
	read -p "start line (defaul 1) ? " START_LINE;
else
	START_LINE=$3;
fi;

# vérifie que le numéro de ligne est correcte
if [[ -z $START_LINE ]]; then
	START_LINE=1;
else
	if [[ $START_LINE =~ [0-9]+ ]]; then
		if [[ $START_LINE -le 0 ]]; then
			START_LINE=1;
		fi;
	else
		echo "start line \"$START_LINE\" is not a positiv number.";
		exit;
	fi;
fi;

# récupère le quatrième argument (la ligne de fin)
if [[ $# -le 3 ]]; then
	read -p "end line (defaul $URL_FILE_TOTAL_CONTENT_SIZE) ? " END_LINE;
else
	END_LINE=$4;
fi;

# vérifie que le numéro de ligne est correcte
if [[ -z $END_LINE ]]; then
	END_LINE=$URL_FILE_TOTAL_CONTENT_SIZE;
else
	if [[ $END_LINE =~ [0-9]+ ]]; then
		if [[ $END_LINE -lt $START_LINE ]]; then
			echo "end line \"$END_LINE\" is not greater than start line.";
			exit;
		fi;
	else
		echo "end line \"$END_LINE\" is not a positiv number.";
		exit;
	fi;
fi;

# reconsidérer la plage e donnée à traiter
URL_FILE_CONTENT=($(sed -n -e "$START_LINE,$END_LINE p" $URL_FILE));
URL_FILE_SUB_CONTENT_SIZE=${#URL_FILE_CONTENT[@]};

# reconsidérer le nombre d'apk par groupe si il est supérieur au nombre d'apk à traiter
if [[ $URL_FILE_SUB_CONTENT_SIZE -lt $STEP ]]; then
	STEP=$URL_FILE_SUB_CONTENT_SIZE;
fi;



#####################################
# Output Directories				#
#####################################
# définition du répertoire racine du projet
readonly ROOT_DIRECTORY=$(eval $(GetVar ROOT_DIRECTORY));

# définition du répertoire de log
readonly LOG_DIRECTORY=$ROOT_DIRECTORY/$(GetVar LOG_DIRECTORY);
mkdir -p $LOG_DIRECTORY;

# définition du répertoire temporaire
readonly TMP_DIRECTORY=$ROOT_DIRECTORY/$(GetVar TMP_DIRECTORY);
mkdir -p $TMP_DIRECTORY;

# définition du répertoire temporaire android
readonly ANDROID_DIRECTORY=$ROOT_DIRECTORY/$(GetVar ANDROID_DIRECTORY);
mkdir -p $ANDROID_DIRECTORY;

# définition du répertoire de stockage des résultats
readonly RESULT_DIRECTORY=$ROOT_DIRECTORY/$(GetVar VIRUSTOTAL_RESULT_DIRECTORY);
mkdir -p $RESULT_DIRECTORY;



#####################################
# Output Files						#
#####################################
# définir le journal
readonly LOG_FILE=$LOG_DIRECTORY/$(basename $0 | rev | cut -d "." -f 2- | rev).$(GetVar LOG_FILE_EXTENSION);

# définir le fichier de résultat
readonly RESULT_FILE=$RESULT_DIRECTORY/$(basename $URL_FILE | rev | cut -d "." -f 2- | rev | cut -d "_" -f 1).$(GetVar DATA_FILE_EXTENSION);

if [[ ! -f $RESULT_FILE ]]; then
	echo "package;suspicious;harmless;malicious;undetected" > $RESULT_FILE;
fi;

# définir le fichier qui concerve le nombre de requête effectué pour la journée
readonly LIMIT_COUNT_FILE=$(mktemp $TMP_DIRECTORY/XXXXXXXXXX);



#####################################
# API Limit							#
#####################################
# définir la limite de requête pour chaque jour
readonly REQUEST_LIMIT_PER_DAY=$(GetVar REQUEST_LIMIT_PER_DAY);

# définir le temps d'attente entre deux requêtes
readonly TIME_BETWEEN_TWO_REQUEST=$(GetVar TIME_BETWEEN_TWO_REQUEST);

# définir la taille limite de transfer de fichier (pour un transfer normal)
readonly FILE_TRANSFER_SIZE_LIMIT=$(GetVar FILE_TRANSFER_SIZE_LIMIT);

# définir la date de lancement du programme
STARTING_DATE=$(date +"%F");

# définir le nombre de requête restante pour aujourd'hui
REMAINING_REQUESTS=$REQUEST_LIMIT_PER_DAY;

#  écrire dans le journal
Log "START" "PROCESS" "INFO" "main process start.";

# récupération du quota de la journée
DownloadRemainingRequest;

# créer le fichier qui stock le nombre de requête utilisables
WriteRemainingRequest $REMAINING_REQUESTS;

# vérifier si il est encore possible de faire des requêtes
if [[ $REMAINING_REQUESTS -le 0 ]]; then
	echo "The request limit for the day has been reached.";
	echo "To continue, choose another API key or wait tomorrow.";
	ForcedStop;
fi;



#####################################
# Processing						#
#####################################
# traiter tous les apk
for ((i=0; i<$URL_FILE_SUB_CONTENT_SIZE; i+=$STEP));
do
	# temporiser entre chaque groupe d'apk
	if [[ $i -ne 0 ]]; then
		sleep $TIME_BETWEEN_TWO_REQUEST"s";
	fi;

	# traiter un groupe d'apk
	for ((j=0; j<$STEP; j+=1));
	do

		# calcul de l'index courant
		INDEX=$(($i+$j));

		# vérifier que l'index n'excède pas le nombre total d'apk
		if [[ $INDEX -lt $URL_FILE_SUB_CONTENT_SIZE ]]; then

			# récupérer les informations de l'application
			PACKAGE_NAME=$(echo ${URL_FILE_CONTENT[$INDEX]} | cut -d ";" -f 1);
			PACKAGE=$ANDROID_DIRECTORY/$PACKAGE_NAME.$(GetVar APK_FILE_EXTENSION);
			URL=$(echo ${URL_FILE_CONTENT[$INDEX]} | cut -d ";" -f 2);
			INDEX=$(($INDEX + $START_LINE));
			
			# vérifier que le jour est bien le même
			if [[ $STARTING_DATE = $(date +"%F") ]]; then

				# vérifier si il est encore possible de faire des requêtes
				if [[ $(ReadRemainingRequest) -le 0 ]]; then
					echo "The request limit for the day has been reached.";
					echo "To continue, choose another API key or wait tomorrow.";
					ForcedStop;
				else
					# lancer le téléchargement
					DownloadApk;

					# compte le nombre d'erreur
					ERROR=$(($ERROR + $?));
				fi;
			else
				# mettre à jour la valeurs dans le fichier
				WriteRemainingRequest $REQUEST_LIMIT_PER_DAY;

				# lancer le téléchargement
				DownloadApk;
				
				# compte le nombre d'erreur
				ERROR=$(($ERROR + $?));
			fi;
		else
			# sortir de la boucle (fin de liste d'apk)
			break;
		fi;

		# si il y a trop d'erreur, arrêt du programme
		if [[ $ERROR -eq $ERROR_MAX ]]; then
			ForcedStop
		fi;
	done;
done;

# attendre la fin complète de l'exécution du programme avant de rendre la main
wait;

# afficher un message de fin dans la console (sortie standart)
echo "----------";
echo "execution finished."

#  écrire dans le journal
Log "FINISH" "PROCESS" "INFO" "main process end.";

# supprimer le fichier temporaire
rm -f $LIMIT_COUNT_FILE;
#!/bin/bash


#############################################################################
# FUNCTIONS																	#
#############################################################################
#####################################
# System							#
#####################################
# tue le processus via son PID
function ForcedStop() {
	local trigger="it self";
	
	if [[ $# -eq 1 ]]; then
		trigger=$1;
	fi;

	# écrire dans le journal
	Log "KILL" "PROCESS" "INFO" "force to kill main process and subprocesses.";

	# calculer le nomnbre de processus en cours d'exécution
	SUB_PROCESS=$(ps --ppid $$ -o comm | grep $(basename $0) -o | wc -w);

	# afficher un message dans la console (sortie standart) avant de forcer l'arrêt du programme
	echo -e "\nforced program to stop by $trigger.";
	echo "To resume, please execute this command: $0 $URL_FILE $API_KEY_FILE $(($i + $START_LINE - $SUB_PROCESS + 1)) $END_LINE";
	
	# supprimer le fichier temporaire
	rm -f $LIMIT_COUNT_FILE;
	kill -9 $$;
}



#####################################
# Env								#
#####################################
# récupère les variables d'environnement qui sont dans le fichier ".env"
function GetVar() {
	echo $(grep $1 $ENV | cut -d "=" -f 2- | xargs);
}



#####################################
# Log								#
#####################################
# écrit dans le fichier de log les actions en rapport direct avec les apk
function LogApk() {
	local ACTION=$1;
	local TAG=$2;
	local ACTION_STATUS=$3;
	local MESSAGE=$4;
	local SIZE=$(echo -n $SOURCE_FILE_TOTAL_CONTENT_SIZE | wc -c);
	local INDEX=$(printf "%"$SIZE"s" $i);

	if [[ ! -v HTTP_STATUS ]]; then
		HTTP_STATUS="---";
	fi;

	FORMAT="[$(date +"%F %T")] $HTTP_STATUS %-8s %-8s %-7s |%"$SIZE"s/%"$SIZE"s| %-50s \"$MESSAGE\"\n";
	printf "$FORMAT" $ACTION $TAG $ACTION_STATUS $INDEX $SOURCE_FILE_TOTAL_CONTENT_SIZE $PACKAGE_NAME >> $LOG_FILE;
}


# écrit dans le fichier de log les actions en rapport direct avec les apk
function Log() {
	local ACTION=$1;
	local TAG=$2;
	local ACTION_STATUS=$3;
	local MESSAGE=$4;
	local SIZE=$(echo -n $URL_FILE_TOTAL_CONTENT_SIZE | wc -c);
	SIZE=$(echo "$SIZE * 2 + 3" | bc);
	
	if [[ ! -v HTTP_STATUS ]]; then
		HTTP_STATUS="---";
	fi;

	FORMAT="[$(date +"%F %T")] $HTTP_STATUS %-8s %-8s %-7s %$SIZE""s %-50s \"$MESSAGE\"\n";
	printf "$FORMAT" $ACTION $TAG $ACTION_STATUS " " >> $LOG_FILE;
}


#############################################################################
# RUN																		#
#############################################################################
#####################################
# System							#
#####################################
# capture le signale Ctrl+C pour forcer "proprement" l'arrêt du programme
trap "ForcedStop 'user'" 2;



#####################################
# Env								#
#####################################
# récupération du fichier d'environnement
readonly ENV=$(realpath $0 | rev | cut -d "/" -f 2- | rev)/.env;

# vérifie que le fichier d'environnement existe
if [[ ! -f $ENV ]]; then
	echo "env file not exist.";
	exit;
fi;



#####################################
# Data								#
#####################################
# le fichier source
declare -x SOURCE_FILE_PATH;

# le contenu du fichier source
declare -x SOURCE_FILE_CONTENT;

# la taille total de données dans le fichier source
declare -x SOURCE_FILE_TOTAL_CONTENT_SIZE;

# la taille des données à traiter
declare -x SOURCE_FILE_TOTAL_SUB_SIZE;

# la première ligne à traiter
declare -x START_LINE=1;

# la dernière ligne à traiter
declare -x END_LIGNE;



#####################################
# Args								#
#####################################
# récupère le premier argument (le fichier csv source)
if [[ $# -eq 0 ]]; then
	read -p "source csv file ? " SOURCE_FILE_PATH;		
else
	SOURCE_FILE_PATH=$1;
fi;

# vérifie que le fichier csv existe
if [[ ! -f $SOURCE_FILE_PATH ]]; then
	echo "fichier \"$SOURCE_FILE_PATH\" inexistant.";
	exit;
else
	SOURCE_FILE_CONTENT=($(sed -n -e "$START_LINE,$(($(wc -l < $SOURCE_FILE_PATH) + 1)) p" $SOURCE_FILE_PATH));
	SOURCE_FILE_TOTAL_CONTENT_SIZE=$(($START_LINE-1 + ${#SOURCE_FILE_CONTENT[@]}));
	SOURCE_FILE_TOTAL_SUB_SIZE=$SOURCE_FILE_TOTAL_CONTENT_SIZE;
fi;

# récupère le deuxième argument (la ligne de début)
if [[ $# -le 1 ]]; then
	read -p "start line (defaul 1) ? " START_LINE;
else
	START_LINE=$2;
fi;

# vérifie que le numéro de ligne est correcte
if [[ -z $START_LINE ]]; then
	START_LINE=1;
else
	if [[ $START_LINE =~ [0-9]+ ]]; then
		if [[ $START_LINE -le 0 ]]; then
			START_LINE=1;
		fi;
	else
		echo "start line \"$START_LINE\" is not a positiv number.";
		exit;
	fi;
fi;

# récupère le troisième argument (la ligne de fin)
if [[ $# -le 2 ]]; then
	read -p "end line (defaul $SOURCE_FILE_TOTAL_CONTENT_SIZE) ? " END_LINE;
else
	END_LINE=$3;
fi;

# vérifie que le numéro de ligne est correcte
if [[ -z $END_LINE ]]; then
	END_LINE=$SOURCE_FILE_TOTAL_CONTENT_SIZE;
else
	if [[ $END_LINE =~ [0-9]+ ]]; then
		if [[ $END_LINE -le $START_LINE ]]; then
			echo "end line \"$END_LINE\" is not greater than start line.";
			exit;
		fi;
	else
		echo "end line \"$END_LINE\" is not a positiv number.";
		exit;
	fi;
fi;

# reconsidérer la plage e donnée à traiter
SOURCE_FILE_CONTENT=($(sed -n -e "$START_LINE,$END_LINE p" $SOURCE_FILE_PATH));
SOURCE_FILE_SUB_CONTENT_SIZE=${#SOURCE_FILE_CONTENT[@]};



#####################################
# Output Directories				#
#####################################
# définition du répertoire racine du projet
readonly ROOT_DIRECTORY=$(eval $(GetVar ROOT_DIRECTORY));

# définition du répertoire de log
readonly LOG_DIRECTORY=$ROOT_DIRECTORY/$(GetVar LOG_DIRECTORY);
mkdir -p $LOG_DIRECTORY;

# définition du répertoire temporaire
readonly TMP_DIRECTORY=$ROOT_DIRECTORY/$(GetVar TMP_DIRECTORY);
mkdir -p $TMP_DIRECTORY;

# définition du répertoire de stockage des résultats
readonly RESULT_DIRECTORY=$ROOT_DIRECTORY/$(GetVar EVOZI_RESULT_DIRECTORY);
mkdir -p $RESULT_DIRECTORY;



#####################################
# Output Files						#
#####################################
# le journal
readonly LOG_FILE=$LOG_DIRECTORY/$(basename $0 | rev | cut -d "." -f 2- | rev).$(GetVar LOG_FILE_EXTENSION);

# le fichier de résultat
readonly RESULT_FILE_SUCCESS=$RESULT_DIRECTORY/$(basename $SOURCE_FILE_PATH | rev | cut -d "." -f 2- | rev | cut -d "_" -f 1)$(GetVar RESULT_FILE_SUCCESS).$(GetVar DATA_FILE_EXTENSION);
readonly RESULT_FILE_NOTFOUND=$RESULT_DIRECTORY/$(basename $SOURCE_FILE_PATH | rev | cut -d "." -f 2- | rev | cut -d "_" -f 1)$(GetVar RESULT_FILE_NOTFOUND).$(GetVar DATA_FILE_EXTENSION);
readonly RESULT_FILE_ERRORS=$RESULT_DIRECTORY/$(basename $SOURCE_FILE_PATH | rev | cut -d "." -f 2- | rev | cut -d "_" -f 1)$(GetVar RESULT_FILE_ERRORS).$(GetVar DATA_FILE_EXTENSION);

if [[ ! -f $RESULT_FILE_SUCCESS ]]; then head -n 1 $SOURCE_FILE_PATH > $RESULT_FILE_SUCCESS; fi;
if [[ ! -f $RESULT_FILE_NOTFOUND ]]; then head -n 1 $SOURCE_FILE_PATH > $RESULT_FILE_NOTFOUND; fi;
if [[ ! -f $RESULT_FILE_ERRORS ]]; then head -n 1 $SOURCE_FILE_PATH > $RESULT_FILE_ERRORS; fi;



#####################################
# Processing						#
#####################################
# écrire dans le journal
Log "STARTED" "PROCESS" "INFO" "main process starting.";

for i in ${!SOURCE_FILE_CONTENT[@]}
do

	ROW=${SOURCE_FILE_CONTENT[$i]};
	PACKAGE_NAME=$(echo $ROW | cut -d ";" -f 1);

	# récupérer la page
	HTTP_STATUS=$(curl -s --request GET -o /dev/null "https://play.google.com/store/apps/details?id=$PACKAGE_NAME" -w "%{http_code}");

	case $HTTP_STATUS in
		200)
			# succès
			# écrire dans le journal
			LogApk "DOWNLOAD" "PAGE" "INFO" "the apk still exist.";

			# sauvegarde
			echo $ROW >> $RESULT_FILE_SUCCESS;
			
			# écriture dans le journal
			LogApk "WRITE" "DATA" "INFO" "data saved in file $RESULT_FILE_SUCESS.";
		;;
		404)
			# page inexistante
			# écrire dans le journal
			LogApk "DOWNLOAD" "PAGE" "WARNING" "the apk does not exist again.";

			# sauvegarde
			echo $ROW >> $RESULT_FILE_NOTFOUND;
			
			# écriture dans le journal
			LogApk "WRITE" "DATA" "INFO" "data saved in file $RESULT_FILE_NOTFOUND.";
		;;
		*)
			# erreur
			# écrire dans le journal
			LogApk "DOWNLOAD" "PAGE" "ERROR" "one error was occured.";

			# sauvegarde
			echo $ROW >> $RESULT_FILE_ERRORS;
			
			# écriture dans le journal
			LogApk "WRITE" "DATA" "INFO" "data saved in file $RESULT_FILE_ERRORS.";
		;;
	esac;
done;

# écrire dans le journal
Log "FINISHED" "PROCESS" "INFO" "process ending.";

# afficher un message de fin dans la console (sortie standart)
echo "----------";
echo "execution finished."
#!/bin/bash


#############################################################################
# FUNCTIONS																	#
#############################################################################
#####################################
# Env								#
#####################################
# récupère les variables d'environnement qui sont dans le fichier ".env"
function GetVar() {
	echo $(grep $1 $ENV | cut -d "=" -f 2- | xargs);
}



#############################################################################
# RUN																		#
#############################################################################
#####################################
# Env								#
#####################################
# récupération du fichier d'environnement
readonly ENV=$(realpath $0 | rev | cut -d "/" -f 2- | rev)/.env;

# vérifie que le fichier d'environnement existe
if [[ ! -f $ENV ]]; then
	echo "env file not exist.";
	exit;
fi;



#####################################
# Directories						#
#####################################
# définition du répertoire racine du projet
readonly ROOT_DIRECTORY=$(eval $(GetVar ROOT_DIRECTORY));

# définition du répertoire de travail
readonly R_DIRECTORY=$ROOT_DIRECTORY/$(GetVar R_DIRECTORY);



#####################################
# Processing						#
#####################################
cd $R_DIRECTORY && r launcher.R
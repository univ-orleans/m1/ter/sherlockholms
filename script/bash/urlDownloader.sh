#!/bin/bash


function RefreshToken() {
	curl -s https://apps.evozi.com/apk-downloader/?id= -o $DIRECTORY_TMP/$FILE_TMP;

	# récupération des informations nécessaires
	TIMESTAMP=$(sed -n 196p $DIRECTORY_TMP/$FILE_TMP | cut -d "," -f 1 | cut -d " " -f 21);
	TOKEN=$(sed -n 165p $DIRECTORY_TMP/$FILE_TMP | cut -d " " -f 14 | cut -d "'" -f 2);
	ID_TIMESTAMP=$(sed -n 196p $DIRECTORY_TMP/$FILE_TMP | cut -d " " -f 17);
	ID_TOKEN=$(sed -n 196p $DIRECTORY_TMP/$FILE_TMP | cut -d "," -f 3 | cut -d ":" -f 1 | sed "s/ *//g");
	ID_PACKAGE=$(sed -n 196p $DIRECTORY_TMP/$FILE_TMP | cut -d "," -f 2 | cut -d ":" -f 1 | sed "s/ *//g");
}


function GetUrl() {
	PACKAGE=$1;
	INDEX=$(($2+1));
	TOTAL=$(($3+1));

	REQUEST_DATA="$ID_TIMESTAMP=$TIMESTAMP&$ID_PACKAGE=$PACKAGE&$ID_TOKEN=$TOKEN&fetch=false";
	APK_RESPONSE=$(curl -s -X POST https://api-apk.evozi.com/download --header "Cache-Control: no-cache;Content-Type: application/x-www-form-urlencoded; charset=UTF-8" --data "$REQUEST_DATA");

	# vérification du type de retour
	TYPE_RESPONSE=$(TypeResponse $APK_RESPONSE);

	case $TYPE_RESPONSE in
		0)
			# succès
			echo "[success] $INDEX/$TOTAL $PACKAGE" >> $DIRECTORY_LOG/$FILE_LOG;
			SHA=$(echo $APK_RESPONSE | cut -d "," -f 7 | cut -d "\"" -f 4);
			URL="https:"$(echo $APK_RESPONSE | cut -d "," -f 3 | cut -d "\"" -f 4 | sed "s/[\]//g");
			echo "$PACKAGE;$SHA;$URL" >> $DIRECTORY_DOWNLOAD/$FILE_DOWNLOAD_SUCCESS;
		;;
		1)
			# limit de requète atteinte
			TIME_TO_WAIT=$(echo $APK_RESPONSE | cut -d " " -f 8)$(echo $APK_RESPONSE | cut -d " " -f 9 | head -c 1);
			echo "[limited] $INDEX/$TOTAL (wait $TIME_TO_WAIT)" >> $DIRECTORY_LOG/$FILE_LOG;
			echo $APK_RESPONSE;
			sleep $TIME_TO_WAIT;
			GetUrl $PACKAGE $2 $3;
		;;
		2)
			# apk payant
			echo "[private] $INDEX/$TOTAL $PACKAGE" >> $DIRECTORY_LOG/$FILE_LOG;
			echo $PACKAGE >> $DIRECTORY_DOWNLOAD/$FILE_DOWNLOAD_NOT_FOUND;
		;;
		3)
			# expiration du token
			echo "[ token ] $INDEX/$TOTAL $PACKAGE" >> $DIRECTORY_LOG/$FILE_LOG;
			RefreshToken;
			GetUrl $PACKAGE $2 $3;
		;;
		4)
			# autre erreur
			echo "[ error ] $INDEX/$TOTAL $PACKAGE" >> $DIRECTORY_LOG/$FILE_LOG;
			echo $PACKAGE >> $DIRECTORY_DOWNLOAD/$FILE_DOWNLOAD_ERRORS;
		;;
	esac;
}


function TypeResponse() {
	if [[ $(echo $* | grep "success" -c) -ge 1 ]]; then
		CODE=0; # succès
	else
		if [[ $(echo $* | grep "error" -c) -ge 1 ]]; then
			if [[ $(echo $* | grep "Rate limit" -c) -ge 1 ]]; then
				CODE=1; # limit de requète atteinte
			else
				if [[ $(echo $* | grep "non-free" -c) -ge 1 ]]; then
					CODE=2; # apk payant
				else
					if [[ $(echo $* | grep "Expired or Invalid Token" -c) -ge 1 ]]; then
						CODE=3; # expiration du token
					else
						CODE=4; # autre erreur
					fi;
				fi;
			fi;
		fi;
	fi;
	echo $CODE;
}


# Vérifier le nombre d'argument
if [[ $# -eq 1 || $# -eq 2 ]]; then

	readonly FILE_PATH=$1;
	FILE_NAME=$(basename $FILE_PATH | sed "s/\.[a-z]*$//g");
	readonly DIRECTORY_PROJECT=$(realpath $0 | rev | cut -d/ -f5- | rev);

	readonly LINE_TOTAL=$(wc -l $FILE_PATH | cut -d " " -f 1);
	LINE_START=-1; # première ligne
	if [[ $# -eq 2 ]]; then
		LINE_START=$(($2-2)); # comencer à la n ième ligne (début à 1)
	fi;
	readonly LINES_TO_READ=$(($LINE_TOTAL-$LINE_START));

	# répertoire de génération de fichier
	readonly FILE_DOWNLOAD_SUCCESS=$FILE_NAME"_success.csv";
	readonly FILE_DOWNLOAD_NOT_FOUND=$FILE_NAME"_notfound.csv";
	readonly FILE_DOWNLOAD_ERRORS=$FILE_NAME"_errors.csv";
	readonly DIRECTORY_DOWNLOAD=$DIRECTORY_PROJECT/"data/store/url/googleplay";
	mkdir -p $DIRECTORY_DOWNLOAD;
	rm -f $DIRECTORY_DOWNLOAD/*;

	# logs
	readonly FILE_LOG="urlDownloader.log"
	readonly DIRECTORY_LOG=$DIRECTORY_PROJECT/"log";
	mkdir -p $DIRECTORY_LOG;
	rm -f $DIRECTORY_LOG/$FILE_LOG;

	# répertoire temporaire
	readonly FILE_TMP="index.html";
	readonly DIRECTORY_TMP="/tmp/sherlockholms";
	mkdir -p $DIRECTORY_TMP;

	# initialisation de variable globale
	TIMESTAMP="";
	TOKEN="";
	ID_TIMESTAMP="";
	ID_TOKEN="";
	ID_PACKAGE="";

	# récupérer un nouveau token
	RefreshToken;

	# lister les noms de packages
	readonly FILE_CONTENT=($(tail -n $LINES_TO_READ $FILE_PATH));
	readonly FILE_CONTENT_SIZE=${#FILE_CONTENT[@]};
	for i in ${!FILE_CONTENT[@]}
	do
		GetUrl ${FILE_CONTENT[$i]} $i ${#FILE_CONTENT[@]};
	done;

	# supprimer le répertoire temporaire
	rm -r $DIRECTORY_TMP;
else
	echo "Erreur nombre d'argument: passé=$#, attendu=2";
	echo "Syntaxe: ./main.sh <file_name> [line_to_start]";
fi

#!/usr/bin/python3

##############################################################################
# IMPORTS																	 #
##############################################################################
from typing import List, Tuple
import sys


##############################################################################
# OBJECT																	 #
##############################################################################
class Data:
	
	separator = ";"
	
	def __init__(self, name:str, cluster:List[Tuple[int,int]]):
		self.name = name
		self.cluster = cluster
	
	def __repr__(self):
		return f"Data<{self.name}>"
	
	def correspond(self, index:int) -> bool:
		"""Vérifie que l'index est dans une des plages de `cluster`"""
		for start, end in self.cluster:
			if start <= index <= end:
				return True
		return False


##############################################################################
# FUNCTION																	 #
##############################################################################
def dataFilter(path_file_in:str, path_file_out:str, data_list:List[Data]):
	"""Génère un nouveau fichier depuis un fichier source avec pour différence un filtrage de données."""
	with open(file=path_file_out, mode="w", encoding="utf-8") as file_out:
		with open(file=path_file_in, mode="r", encoding="utf-8") as file_in:
			for row in file_in.readlines():
				row = row[:-1]
				unfiltered_row = row.split(Data.separator)
				filtered_row = list()
				for i in range(len(unfiltered_row)):
					for data in data_list:
						if data.correspond(i+1):
							filtered_row.append(unfiltered_row[i] if len(unfiltered_row[i]) > 0 else "0")
				file_out.write(Data.separator.join(filtered_row) + "\n")


##############################################################################
# RUN																		 #
##############################################################################
if __name__ == "__main__":
	arg_number = len(sys.argv)

	path_file_in = str()
	path_file_out = str()
	data_list = list()

	# liste des filtres applicables
	all_data = [
		Data("Package", [(1,1)]),
		Data("Category", [(2,2)]),
		Data("Class", [(3,3)]),
		Data("Feature", [(4,176)]),
		Data("Permission", [(177,401), (1302,1331)]),
		Data("Unknow", [(402,1301)]),
		Data("Intent", [(1332,1439)])
	]

	# récupération du fichier csv d'entrée
	if arg_number == 1:
		path_file_in = input("File in: ")
	else:
		path_file_in = sys.argv[1]
	
	# récupération du fichier csv de sortie
	if arg_number <= 2:
		path_file_out = input("File out: ")
	else:
		path_file_out = sys.argv[2]
	
	# récupération des filtres
	if arg_number <= 3:
		print("Chose filters (e.g 1 2 3 4):")
		for i in range(len(all_data)):
			print(f"{i+1}: {all_data[i].name}")
		data_list = [ all_data[int(i)-1] for i in input("Filters: ").split(" ") ]
	else:
		data_list = [all_data[int(i)-1] for i in sys.argv[3:]]

	# affichage de la différence entre les catégories de départ, et celle de fin
	print("From:", ";".join(map(lambda d: d.name.lower(), all_data)))
	print("To:  ", ";".join(map(lambda d: d.name.lower() if d in data_list else "\033[91m" + d.name.lower() + "\033[0m", all_data)))

	# filtrage des données
	dataFilter(
		path_file_in=path_file_in,
		path_file_out=path_file_out,
		data_list=data_list
	)